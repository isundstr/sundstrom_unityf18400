﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PlayerControllerMod : Unit
{
	public float speed;
    public float turnSpeed;
	public float tilt;
	public Done_Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	 
	private float nextFire;

    float boostCharge = 0f;


    public GameObject playerExplosion;

    float speedBoost = 0f;

    Rigidbody rb;

    public Image healthBar;

    int maxHealth = 5;

    private void Start()
    {
        health = maxHealth;
        RefreshHealth();

        rb = GetComponent<Rigidbody>();
    }

    void Update ()
	{
		if (Input.GetButton("Fire1") && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			GameObject bolt = Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            bolt.GetComponent<Done_Mover>().speed += speedBoost; 
			GetComponent<AudioSource>().Play ();
		}

        if (Input.GetKey(KeyCode.LeftShift))
        {
            boostCharge = Mathf.MoveTowards(boostCharge, 1f, Time.unscaledDeltaTime);
            print("boostCharge: " + boostCharge);
            Time.timeScale = Mathf.Lerp(Time.timeScale, .5f, boostCharge);
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Time.timeScale = 1f;
            boostCharge = 0f;
            speedBoost = 20f;
        }

        GameController.gc.camScript.zoom = boostCharge;
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

        speedBoost = Mathf.MoveTowards(speedBoost, 0f, Time.unscaledDeltaTime * 5f);

		//Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = moveVertical * transform.forward * (speed + speedBoost);
        rb.angularVelocity = new Vector3(0f, moveHorizontal * turnSpeed, 0f);

		/*
		GetComponent<Rigidbody>().position = new Vector3
		(
			Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax), 
			0.0f, 
			Mathf.Clamp (GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
		);*/
		
		//GetComponent<Rigidbody>().rotation = Quaternion.Euler (0.0f, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("EnemyBolt"))
        {

            Destroy(other.gameObject);
            if (TakeDamage(1))
            {
                Death();
            }

            RefreshHealth();
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            Death();
            if (other.gameObject.GetComponent<Unit>() != null)
                other.gameObject.GetComponent<Unit>().Death();
        }
    }

    override public void Death()
    {
        base.Death();
    }

    void RefreshHealth()
    {

        StopCoroutine("AnimateHealth");
        StartCoroutine("AnimateHealth");
    }

    IEnumerator AnimateHealth()
    {
        healthBar.enabled = true;

        float frameRate = 1f / 40f;
        float goalHealth = (float)(health) / (float)(maxHealth) * .5f;
        while (healthBar.fillAmount != goalHealth)
        {
            healthBar.fillAmount = Mathf.MoveTowards(healthBar.fillAmount, goalHealth, frameRate * .5f);
            Color useColor = Color.Lerp(Color.red, Color.green, healthBar.fillAmount / .5f);
            healthBar.color = useColor;
            yield return new WaitForSeconds(frameRate);
        }

        yield return new WaitForSeconds(1f);

        healthBar.enabled = false;
    }
}
