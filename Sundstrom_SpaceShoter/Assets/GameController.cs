﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController gc;

    public GameObject playerShip;

    public GameObject enemyPrefab;

    public Transform mapCanvas;

    public CamFollow camScript;

    // Use this for initialization
    void Awake() {
        if (gc != null)
        {
            Destroy(gc.gameObject);
        }
        else
            gc = this;

    }

    private void Start()
    {
        SpawnEnemies();
    }

    // Update is called once per frame
    void Update() {

    }

    void SpawnEnemies()
    {
        int enemyCount = Random.Range(10, 15);

 

        for(int i = 0; i < enemyCount; i++)
        {
            Vector2 angle = Random.insideUnitCircle.normalized;
            angle = angle * Random.Range(10f, 250f);
            Vector3 randomPosition = new Vector3(angle.x, 0f, angle.y);
            Instantiate(enemyPrefab, randomPosition, enemyPrefab.transform.rotation);
        }

    }

    void Death()
    {

    }
}
