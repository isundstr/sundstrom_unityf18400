﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapObjectScript : MonoBehaviour {
    public GameObject markerPrefab;
    public RectTransform myMarker;
    // Use this for initialization
    Image img;
	void Start () {
        GameObject marker  = Instantiate(markerPrefab, GameController.gc.mapCanvas) as GameObject;
        myMarker = marker.GetComponent<RectTransform>();
        img = marker.GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 currentView = Camera.main.transform.position;

        Vector3 diff =  new Vector3(transform.position.x, currentView.y, transform.position.z) - currentView;

        float angle = Vector3.Angle(Vector3.back, diff);

        if (diff.x < 0f)
            angle = -angle;

        myMarker.localRotation = Quaternion.Euler(new Vector3(0f, 0f, angle));

        myMarker.localScale = Vector3.one * (1f + (1f-Mathf.Clamp((diff.magnitude/200f), 0f, 1f)) * 2f);

        if (diff.magnitude < 9f)
            img.enabled = false;
        else
            img.enabled = true;

        diff = diff.normalized;

        myMarker.anchoredPosition = new Vector3(diff.x, diff.z, 0f) * 300f;
	}

    public void End()
    {
        Destroy(myMarker.gameObject);
    }
}
