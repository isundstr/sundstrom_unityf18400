﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour {
    public Transform player;
    Rigidbody pRB;
    float camMaxSpeed = 8f;
    float camVelocity;

    public float zoom = 0f;

	// Use this for initialization
	void Start () {
        pRB = player.gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (player == null)
            return;

        Vector3 target = new Vector3(player.position.x, 0f, player.position.z);
        Vector3 usePos = new Vector3(transform.position.x, 0f, transform.position.z);


        usePos = Vector3.Lerp(usePos, target, Time.unscaledDeltaTime * 2f);
   

        float clamp = 3f;
        if (Vector3.Distance(target, usePos) > clamp)
        {
            Vector3 dir = usePos - target;
            usePos = target + dir.normalized * clamp;
        }


        float zoomLevel = Mathf.Lerp(15f, 15f * .5f, zoom);

        usePos = Vector3.Lerp(usePos, target, zoom);

 

        transform.position = new Vector3(usePos.x, zoomLevel, usePos.z);
    }
}
