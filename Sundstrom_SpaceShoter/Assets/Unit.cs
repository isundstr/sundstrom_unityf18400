﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour {

    public int health;
    public GameObject deathExplosion;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    protected bool TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
            return true;
        else
            return false;
    }

    virtual public void Death()
    {
        Instantiate(deathExplosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
