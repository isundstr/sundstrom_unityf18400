﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationAI : Unit {

    public float turnSpeed;

    public GameObject boltPrefab;

    bool inRange = false;

    public Transform shotPoint;


	// Use this for initialization
	void Start () {
        Invoke("Fire", 2f);
	}
	
	// Update is called once per frame
	void Update () {
		

        if (GameController.gc.playerShip != null)
        {
            if (Vector3.Distance(GameController.gc.playerShip.transform.position, transform.position) < 20f)
            {
                inRange = true;
                Quaternion lookRot = Quaternion.LookRotation(GameController.gc.playerShip.transform.position - transform.position, Vector3.up);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRot, turnSpeed * Time.deltaTime);
            }
            else
                inRange = false;

        }
	}

    void Fire()
    {
        if (inRange)
        {
            Instantiate(boltPrefab, shotPoint.transform.position, shotPoint.transform.rotation);
        }

        Invoke("Fire", 2f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerBolt"))
        {

            Destroy(other.gameObject);

            if (TakeDamage(1))
            {
                Death();
            }
        }

    }


    override public void Death()
    {
        GetComponent<MapObjectScript>().End();
        base.Death();

    }
}
